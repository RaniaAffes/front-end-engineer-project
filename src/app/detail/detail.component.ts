import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Planet } from '../Models/Planet';
import { PlanetsService } from '../Services/Planets/planets.service';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {

  item: Planet=new Planet();

  constructor(private router: ActivatedRoute,
    private _planetService: PlanetsService) { }

  ngOnInit() {
    let id=this.router.snapshot.params['id'];
    this._planetService.getById(id).then((res)=>{
      this.item=res;
    })
  }

}
