import { Planet } from './../Models/Planet';
import { PlanetsService } from './../Services/Planets/planets.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  list:Planet[]=[];

  constructor(private _planetService: PlanetsService,
    private router: Router) { }

  ngOnInit() {
    this._planetService.getList().then((res:any)=>{
      this.list=res.results;

    })
  }

  detail(item) {
    let url=item.url;
    let id=url.substring(url.length-2, url.length-1);
    this.router.navigate(['detail', id]);
  }

}
