import { PlanetsService } from './Services/Planets/planets.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ListComponent } from './list/list.component';
import { DetailComponent } from './detail/detail.component';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { InterfaceComponent } from './interface/interface.component';

@NgModule({
  declarations: [
    AppComponent,
    ListComponent,
    DetailComponent,
    InterfaceComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [PlanetsService, HttpClient],
  bootstrap: [AppComponent]
})
export class AppModule { }
