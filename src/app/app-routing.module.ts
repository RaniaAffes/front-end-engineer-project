import { InterfaceComponent } from './interface/interface.component';
import { DetailComponent } from './detail/detail.component';
import { ListComponent } from './list/list.component';
import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  // {
  //   path:'list',
  //   component: ListComponent
  // },
  // {
  //   path:'detail/:id',
  //   component: DetailComponent
  // },
  {
    path:'interface',
    component: InterfaceComponent
  },
  {
    path: '',
    pathMatch:'full',
    redirectTo:'/interface'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
